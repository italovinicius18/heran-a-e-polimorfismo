#ifndef  HEXAGONO_HPP
#define  HEXAGONO_HPP

#include <string>
#include "formageometrica.hpp"

using namespace std;

class Hexagono : public FormaGeometrica
{
private:
    double base;
    double altura;
public:
    Hexagono();
    ~Hexagono();
    Hexagono(string tipo, double base, double altura);
    double calcula_perimetro();
    double calcula_area();
};
#endif