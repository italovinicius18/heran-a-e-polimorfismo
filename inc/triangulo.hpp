#ifndef  TRIANGULO_HPP
#define  TRIANGULO_HPP

#include <string>
#include "formageometrica.hpp"

using namespace std;

class Triangulo : public FormaGeometrica
{
private:

public:
    Triangulo();
    ~Triangulo();
    Triangulo(string tipo, double base,double altura);
    double calcula_perimetro();
    double calcula_area();
};
#endif