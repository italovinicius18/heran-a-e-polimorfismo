#ifndef CIRCULO_HPP
#define CIRCULO_HPP

#include <string>
#include "formageometrica.hpp"

using namespace std;

class Circulo : public FormaGeometrica
{
private:
    double base;
public:
    Circulo();
    ~Circulo();
    Circulo(string tipo,double base);
    double calcula_perimetro();
    double calcula_area(); //valor recebido é o diametro    
};
#endif