#ifndef FORMAGEOMETRICA_HPP
#define FORMAGEOMETRICA_HPP

#include <string>

using namespace std;

class FormaGeometrica {
private:
    string tipo;
    double base;
    double altura;
public:
    FormaGeometrica();
    FormaGeometrica(double base, double altura);
    FormaGeometrica(string tipo, double base, double altura);
    ~FormaGeometrica();
    void set_tipo(string tipo);
    string get_tipo();
    void set_base(double base);
    double get_base();
    void set_altura(double altura);
    double get_altura();
    virtual double calcula_area();
    virtual double calcula_perimetro();
};

#endif