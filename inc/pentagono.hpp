#ifndef  PENTAGONO_HPP
#define  PENTAGONO_HPP

#include <string>
#include "formageometrica.hpp"

using namespace std;

class Pentagono : public FormaGeometrica
{
private:
    double base;
    double altura;
public:
    Pentagono();
    ~Pentagono();
    Pentagono(string tipo, double base, double altura);
    double calcula_perimetro();
    double calcula_area();
};
#endif