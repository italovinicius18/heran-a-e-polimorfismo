#ifndef  PARALELOGRAMO_HPP
#define  PARALELOGRAMO_HPP

#include <string>
#include "formageometrica.hpp"

using namespace std;

class Paralelogramo : public FormaGeometrica
{
private:

public:
    Paralelogramo();
    ~Paralelogramo();
    Paralelogramo(string tipo, double base,double altura);
};
#endif