#ifndef  QUADRADO_HPP
#define  QUADRADO_HPP

#include <string>
#include "formageometrica.hpp"

using namespace std;

class Quadrado : public FormaGeometrica
{
private:
    double base;
    double altura;
public:
    Quadrado();
    ~Quadrado();
    Quadrado(string tipo, double base,double altura);
};
#endif