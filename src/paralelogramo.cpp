#include "paralelogramo.hpp"
#include <iostream>

Paralelogramo::Paralelogramo(){
    set_tipo("Paralelogramo");
}

Paralelogramo::~Paralelogramo(){
        
}

Paralelogramo::Paralelogramo(string tipo, double base, double altura){
    set_tipo(tipo);
    set_base(base);
    set_altura(altura);
}
