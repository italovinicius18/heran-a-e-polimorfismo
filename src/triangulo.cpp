#include "triangulo.hpp"
#include <iostream>

Triangulo::Triangulo(){
    set_tipo("Triangulo equilátero");
}

Triangulo::Triangulo(string tipo, double base, double altura){
    set_tipo(tipo);
    set_base(base);
    set_altura(altura);
}

Triangulo::~Triangulo(){
        
}

double Triangulo:: calcula_perimetro(){
        return get_base()*3;
}
double Triangulo:: calcula_area(){
        return (get_base()*get_altura())/2;
}