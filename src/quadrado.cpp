#include "quadrado.hpp"
#include <iostream>

Quadrado::Quadrado(){
    set_tipo("Quadrado");
}

Quadrado::~Quadrado(){
        
}

Quadrado::Quadrado(string tipo, double base, double altura){
    set_tipo(tipo);
    set_base(base);
    set_altura(altura);
}

