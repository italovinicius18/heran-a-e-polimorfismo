#include "hexagono.hpp"
#include <bits/stdc++.h>

Hexagono::Hexagono(){
    set_tipo("Hexágono regular");
}

Hexagono::Hexagono(string tipo, double base, double altura){
    set_tipo(tipo);
    set_base(base);
    set_altura(altura);
}

Hexagono::~Hexagono(){
        
}

double Hexagono:: calcula_perimetro(){
    return get_base()*6;
}
double Hexagono:: calcula_area(){
    return (3*get_base()*get_base()*sqrt(3))/2;
}