#include "circulo.hpp"
#include <iostream>
double pi = 3.14159;

Circulo::Circulo(){
    set_tipo("Círculo");
}

Circulo::Circulo(string tipo, double base){
    set_tipo(tipo);
    set_base(base);
}

Circulo::~Circulo(){
        
}

double Circulo:: calcula_perimetro(){
    double raio = get_base()/2;
    return pi*raio*raio;
}
double Circulo:: calcula_area(){ //valor recebido é o diametro 
    double raio = get_base()/2;
    return 2*pi*raio;
}