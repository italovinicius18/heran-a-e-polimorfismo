#include "pentagono.hpp"
#include <iostream>

Pentagono::Pentagono(){
    set_tipo("Pentágono regular");
}

Pentagono::~Pentagono(){
        
}

Pentagono::Pentagono(string tipo, double base, double altura){
    set_tipo(tipo);
    set_base(base);
    set_altura(altura);
}


double Pentagono:: calcula_perimetro(){
    return get_base()*5;
}
double Pentagono:: calcula_area(){
    return (get_base()*5*get_altura())/2;
}