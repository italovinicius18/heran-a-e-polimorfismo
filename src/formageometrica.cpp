#include "formageometrica.hpp"
#include <iostream>

FormaGeometrica::FormaGeometrica(){
    tipo = "Genérico";
    base = 10.0f;
    altura = 5.0;
}
FormaGeometrica::FormaGeometrica(double base, double altura){
    tipo = "Genérico";
    this->base = base;
    this->altura = altura;
}
FormaGeometrica::FormaGeometrica(string tipo, double base, double altura){
    set_tipo(tipo);
    set_base(base);
    set_altura(altura);
}
FormaGeometrica::~FormaGeometrica(){
    cout << "Destruindo o objeto: " << tipo << endl;
}
void FormaGeometrica::set_tipo(string tipo){
    this->tipo = tipo;
}
string FormaGeometrica::get_tipo(){
    return tipo;
}
void FormaGeometrica::set_base(double base){
    if(base < 0)
        throw(1);
    else
        this->base = base;
}
double FormaGeometrica::get_base(){
    return base;
}
void FormaGeometrica::set_altura(double altura){
    if(altura < 0)
        throw(1);
    else
        this->altura = altura;
}
double FormaGeometrica::get_altura(){
    return altura;
}
double FormaGeometrica::calcula_area(){
    return base * altura;
}
double FormaGeometrica::calcula_perimetro(){
    return 2*base + 2*altura;
}