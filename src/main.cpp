#include <bits/stdc++.h>
#include "circulo.hpp"
#include "hexagono.hpp"
#include "paralelogramo.hpp"
#include "quadrado.hpp"
#include "triangulo.hpp"
#include "pentagono.hpp"

using namespace std;

int main()
{

    Quadrado quadrado;
    Paralelogramo paralelogramo;
    Circulo circulo;
    Triangulo triangulo;
    Hexagono hexagono;
    Pentagono pentagono;

    vector<FormaGeometrica *> formas;

    formas.push_back(new Quadrado("Quadrado", 5, 5));
    formas.push_back(new Paralelogramo("Paralelogramo", 5, 6));
    formas.push_back(new Circulo("Círculo", 10));
    formas.push_back(new Triangulo("Triângulo", 5, 5));
    formas.push_back(new Hexagono("Hexágono", 5, 6));
    formas.push_back(new Pentagono("Pentágono", 7, 8));

    cout << "Lista de formas (Tipo / Perimetro / Área)" << endl;
    for (FormaGeometrica *objeto : formas)
    {
        cout << objeto->get_tipo() << " / " << objeto->calcula_perimetro() << " / " << objeto->calcula_area() << endl;
    }

    return 0;
}